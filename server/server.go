package server

import (
	"encoding/json"
	"fmt"
	"log"
	"net/url"
	"time"

	"github.com/prometheus/client_golang/prometheus"

	"gitlab.com/DasSkelett/LMP-Exporter/common"
)

type Server struct {
	CurrentState struct {
		StartTime      string        `json:"StartTime"`
		CurrentPlayers []interface{} `json:"CurrentPlayers"`
		CurrentVessels []struct {
			ID                       string      `json:"Id"`
			Name                     string      `json:"Name"`
			Type                     string      `json:"Type"`
			DistanceTravelled        float64     `json:"DistanceTravelled"`
			Situation                interface{} `json:"Situation"`
			Lat                      float64     `json:"Lat"`
			Lon                      float64     `json:"Lon"`
			Alt                      float64     `json:"Alt"`
			SemiMajorAxis            float64     `json:"SemiMajorAxis"`
			Eccentricity             float64     `json:"Eccentricity"`
			Inclination              float64     `json:"Inclination"`
			ArgumentOfPeriapsis      float64     `json:"ArgumentOfPeriapsis"`
			LongitudeOfAscendingNode float64     `json:"LongitudeOfAscendingNode"`
			MeanAnomaly              float64     `json:"MeanAnomaly"`
			Epoch                    float64     `json:"Epoch"`
			ReferenceBody            int         `json:"ReferenceBody"`
		} `json:"CurrentVessels"`
		Subspaces []struct {
			ID      int     `json:"Id"`
			Time    float64 `json:"Time"`
			Creator string  `json:"Creator"`
		} `json:"Subspaces"`
		BytesUsed int `json:"BytesUsed"`
	} `json:"CurrentState"`
	// GeneralSettings struct {
	// 	ServerName           string  `json:"ServerName"`
	// 	Description          string  `json:"Description"`
	// 	CountryCode          string  `json:"CountryCode"`
	// 	WebsiteText          string  `json:"WebsiteText"`
	// 	Website              string  `json:"Website"`
	// 	HasPassword          bool    `json:"HasPassword"`
	// 	HasAdminPassword     bool    `json:"HasAdminPassword"`
	// 	Motd                 string  `json:"Motd"`
	// 	MaxPlayers           int     `json:"MaxPlayers"`
	// 	MaxUsernameLength    int     `json:"MaxUsernameLength"`
	// 	AutoDekessler        float64 `json:"AutoDekessler"`
	// 	AutoNuke             float64 `json:"AutoNuke"`
	// 	Cheats               bool    `json:"Cheats"`
	// 	AllowSackKerbals     bool    `json:"AllowSackKerbals"`
	// 	ConsoleIdentifier    string  `json:"ConsoleIdentifier"`
	// 	GameDifficulty       string  `json:"GameDifficulty"`
	// 	GameMode             string  `json:"GameMode"`
	// 	ModControl           bool    `json:"ModControl"`
	// 	NumberOfAsteroids    int     `json:"NumberOfAsteroids"`
	// 	NumberOfComets       int     `json:"NumberOfComets"`
	// 	TerrainQuality       string  `json:"TerrainQuality"`
	// 	SafetyBubbleDistance float64 `json:"SafetyBubbleDistance"`
	// } `json:"GeneralSettings"`
	// WarpSettings struct {
	// 	WarpMode string `json:"WarpMode"`
	// } `json:"WarpSettings"`
	// ServerScreenshotSettings struct {
	// 	MinScreenshotIntervalMs int `json:"MinScreenshotIntervalMs"`
	// 	MaxScreenshotWidth      int `json:"MaxScreenshotWidth"`
	// 	MaxScreenshotHeight     int `json:"MaxScreenshotHeight"`
	// 	MaxScreenshotsPerUser   int `json:"MaxScreenshotsPerUser"`
	// 	MaxScreenshotsFolders   int `json:"MaxScreenshotsFolders"`
	// } `json:"ServerScreenshotSettings"`
	// ServerMasterServerSettings struct {
	// 	RegisterWithMasterServer           bool `json:"RegisterWithMasterServer"`
	// 	MasterServerRegistrationMsInterval int  `json:"MasterServerRegistrationMsInterval"`
	// } `json:"ServerMasterServerSettings"`
	// ServerLogSettings struct {
	// 	LogLevel        string `json:"LogLevel"`
	// 	ExpireLogs      int    `json:"ExpireLogs"`
	// 	UseUtcTimeInLog bool   `json:"UseUtcTimeInLog"`
	// } `json:"ServerLogSettings"`
	// ServerIntervalSettings struct {
	// 	VesselUpdatesMsInterval          int `json:"VesselUpdatesMsInterval"`
	// 	SecondaryVesselUpdatesMsInterval int `json:"SecondaryVesselUpdatesMsInterval"`
	// 	SendReceiveThreadTickMs          int `json:"SendReceiveThreadTickMs"`
	// 	MainTimeTick                     int `json:"MainTimeTick"`
	// 	BackupIntervalMs                 int `json:"BackupIntervalMs"`
	// } `json:"ServerIntervalSettings"`
	// ServerGameplaySettings struct {
	// 	AllowStockVessels                bool    `json:"AllowStockVessels"`
	// 	AutoHireCrews                    bool    `json:"AutoHireCrews"`
	// 	BypassEntryPurchaseAfterResearch bool    `json:"BypassEntryPurchaseAfterResearch"`
	// 	CanRevert                        bool    `json:"CanRevert"`
	// 	CommNetwork                      bool    `json:"CommNetwork"`
	// 	RespawnTime                      float64 `json:"RespawnTime"`
	// 	FundsGainMultiplier              float64 `json:"FundsGainMultiplier"`
	// 	FundsLossMultiplier              float64 `json:"FundsLossMultiplier"`
	// 	IndestructibleFacilities         bool    `json:"IndestructibleFacilities"`
	// 	MissingCrewsRespawn              bool    `json:"MissingCrewsRespawn"`
	// 	ReentryHeatScale                 float64 `json:"ReentryHeatScale"`
	// 	RepGainMultiplier                float64 `json:"RepGainMultiplier"`
	// 	RepLossDeclined                  float64 `json:"RepLossDeclined"`
	// 	RepLossMultiplier                float64 `json:"RepLossMultiplier"`
	// 	ResourceAbundance                float64 `json:"ResourceAbundance"`
	// 	ScienceGainMultiplier            float64 `json:"ScienceGainMultiplier"`
	// 	StartingFunds                    float64 `json:"StartingFunds"`
	// 	StartingReputation               float64 `json:"StartingReputation"`
	// 	StartingScience                  float64 `json:"StartingScience"`
	// 	AllowNegativeCurrency            bool    `json:"AllowNegativeCurrency"`
	// 	PressurePartLimits               bool    `json:"PressurePartLimits"`
	// 	KerbalGToleranceMult             float64 `json:"KerbalGToleranceMult"`
	// 	GPartLimits                      bool    `json:"GPartLimits"`
	// 	GKerbalLimits                    bool    `json:"GKerbalLimits"`
	// 	ActionGroupsAlways               bool    `json:"ActionGroupsAlways"`
	// 	KerbalExp                        bool    `json:"KerbalExp"`
	// 	ImmediateLevelUp                 bool    `json:"ImmediateLevelUp"`
	// 	ObeyCrossfeedRules               bool    `json:"ObeyCrossfeedRules"`
	// 	BuildingDamageMultiplier         float64 `json:"BuildingDamageMultiplier"`
	// 	PartUpgrades                     bool    `json:"PartUpgrades"`
	// 	RequireSignalForControl          bool    `json:"RequireSignalForControl"`
	// 	RangeModifier                    float64 `json:"RangeModifier"`
	// 	DsnModifier                      float64 `json:"DsnModifier"`
	// 	OcclusionModifierVac             float64 `json:"OcclusionModifierVac"`
	// 	OcclusionModifierAtm             float64 `json:"OcclusionModifierAtm"`
	// 	EnableFullSASInSandbox           bool    `json:"EnableFullSASInSandbox"`
	// 	ExtraGroundstations              bool    `json:"ExtraGroundstations"`
	// 	ReentryBlackout                  bool    `json:"ReentryBlackout"`
	// } `json:"ServerGameplaySettings"`
	// ServerDebugSettings struct {
	// 	SimulatedLossChance         float64 `json:"SimulatedLossChance"`
	// 	SimulatedDuplicatesChance   float64 `json:"SimulatedDuplicatesChance"`
	// 	MaxSimulatedRandomLatencyMs int     `json:"MaxSimulatedRandomLatencyMs"`
	// 	MinSimulatedLatencyMs       int     `json:"MinSimulatedLatencyMs"`
	// } `json:"ServerDebugSettings"`
	// ServerCraftSettings struct {
	// 	MinCraftLibraryRequestIntervalMs int `json:"MinCraftLibraryRequestIntervalMs"`
	// 	MaxCraftsPerUser                 int `json:"MaxCraftsPerUser"`
	// 	MaxCraftFolders                  int `json:"MaxCraftFolders"`
	// } `json:"ServerCraftSettings"`
	// ServerConnectionSettings struct {
	// 	Port                    int  `json:"Port"`
	// 	HearbeatMsInterval      int  `json:"HearbeatMsInterval"`
	// 	ConnectionMsTimeout     int  `json:"ConnectionMsTimeout"`
	// 	Upnp                    bool `json:"Upnp"`
	// 	UpnpMsTimeout           int  `json:"UpnpMsTimeout"`
	// 	MaximumTransmissionUnit int  `json:"MaximumTransmissionUnit"`
	// 	AutoExpandMtu           bool `json:"AutoExpandMtu"`
	// } `json:"ServerConnectionSettings"`
	// ServerWebsiteSettings struct {
	// 	EnableWebsite     bool `json:"EnableWebsite"`
	// 	Port              int  `json:"Port"`
	// 	RefreshIntervalMs int  `json:"RefreshIntervalMs"`
	// } `json:"ServerWebsiteSettings"`
}

// List is the type of the received JSON, it's an array with a single Server element.
type List []Server

type allMetrics struct {
	onlinePlayers prometheus.Gauge
	startTime     prometheus.Gauge
	vesselCount   prometheus.Gauge
	subspaceCount prometheus.Gauge
	bytesUsed     prometheus.Gauge
}

type Collector struct {
	URL     url.URL
	metrics allMetrics
}

func NewServerCollector(rawURL string) *Collector {
	parsedURL, err := url.Parse(rawURL)
	if err != nil {
		return nil
	}
	return &Collector{
		URL: *parsedURL,
		metrics: allMetrics{
			onlinePlayers: prometheus.NewGauge(prometheus.GaugeOpts{
				Namespace: "lmp",
				Subsystem: "server",
				Name:      "players_online_total",
				Help:      "Total numbers of players online",
			}),
			startTime: prometheus.NewGauge(prometheus.GaugeOpts{
				Namespace: "lmp",
				Subsystem: "server",
				Name:      "start_time_seconds",
				Help:      "Start time of the server in seconds since Unix Epoch",
			}),
			vesselCount: prometheus.NewGauge(prometheus.GaugeOpts{
				Namespace: "lmp",
				Subsystem: "server",
				Name:      "vessel_count",
				Help:      "Number of vessels handled by the server",
			}),
			subspaceCount: prometheus.NewGauge(prometheus.GaugeOpts{
				Namespace: "lmp",
				Subsystem: "server",
				Name:      "subspace_count",
				Help:      "Number of subspaces handled by the server",
			}),
			bytesUsed: prometheus.NewGauge(prometheus.GaugeOpts{
				Namespace: "lmp",
				Subsystem: "server",
				Name:      "bytes_used",
				Help:      "Number of bytes \"used\" by the server",
			}),
		},
	}
}

func (sc *Collector) Describe(c chan<- *prometheus.Desc) {
	sc.metrics.onlinePlayers.Describe(c)
	sc.metrics.startTime.Describe(c)
	sc.metrics.vesselCount.Describe(c)
	sc.metrics.subspaceCount.Describe(c)
	sc.metrics.bytesUsed.Describe(c)
}

func (sc *Collector) Collect(c chan<- prometheus.Metric) {
	err := sc.collectMetrics()
	if err != nil {
		err = fmt.Errorf("error during master server collect for %s: %w", sc.URL.String(), err)
		log.Println(err)
		return
	}

	sc.metrics.onlinePlayers.Collect(c)
	sc.metrics.startTime.Collect(c)
	sc.metrics.vesselCount.Collect(c)
	sc.metrics.subspaceCount.Collect(c)
	sc.metrics.bytesUsed.Collect(c)
}

func (sc *Collector) collectMetrics() error {
	body, err := common.FetchJSON(sc.URL)
	if err != nil {
		return err
	}
	serverList := make(List, 0, 96)
	err = json.NewDecoder(body).Decode(&serverList)
	if err != nil {
		return err
	}

	server := serverList[0]

	parsedStartTime, err := time.ParseInLocation("2006-01-02T15:04:05", server.CurrentState.StartTime, time.UTC)
	if err != nil {
		return err
	}

	sc.metrics.onlinePlayers.Set(float64(len(server.CurrentState.CurrentPlayers)))
	sc.metrics.startTime.Set(float64(parsedStartTime.Unix()))
	sc.metrics.vesselCount.Set(float64(len(server.CurrentState.CurrentVessels)))
	sc.metrics.subspaceCount.Set(float64(len(server.CurrentState.Subspaces)))
	sc.metrics.bytesUsed.Set(float64(server.CurrentState.BytesUsed))

	return nil
}
