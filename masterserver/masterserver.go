package masterserver

import (
	"encoding/json"
	"fmt"
	"log"
	"net"
	"net/url"
	"strings"

	"github.com/prometheus/client_golang/prometheus"

	"gitlab.com/DasSkelett/LMP-Exporter/common"
)

type Server struct {
	Country           string `json:"Country"`
	ExternalEndpoint  string `json:"ExternalEndpoint"`
	InternalEndpoint6 string `json:"InternalEndpoint6,omitempty"`
	Version           string `json:"Version"`
	Password          bool   `json:"Password"`
	Cheats            bool   `json:"Cheats"`
	GameMode          int    `json:"GameMode"`
	MaxPlayers        int    `json:"MaxPlayers"`
	DedicatedServer   bool   `json:"DedicatedServer"`
	PlayerCount       int    `json:"PlayerCount"`
	ServerName        string `json:"ServerName"`
	Description       string `json:"Description"`
	Website           string `json:"Website"`
	WebsiteText       string `json:"WebsiteText"`
}

type ServerList []Server

type allMetrics struct {
	serversTotal    prometheus.GaugeVec
	serversWithIPv6 prometheus.Gauge
	uniqueIPv4Addr  prometheus.Gauge
	uniqueIPv6Addr  prometheus.Gauge
	onlinePlayers   prometheus.Gauge
}

type Collector struct {
	URL     url.URL
	metrics allMetrics
}

func NewMasterServerCollector(rawURL string) *Collector {
	parsedURL, err := url.Parse(rawURL)
	if err != nil {
		return nil
	}
	return &Collector{
		URL: *parsedURL,
		metrics: allMetrics{
			serversTotal: *prometheus.NewGaugeVec(prometheus.GaugeOpts{
				Namespace: "lmp",
				Subsystem: "masterserver",
				Name:      "servers_total",
				Help:      "Total number of servers registered with this master server",
			}, []string{"country"},
			),
			serversWithIPv6: prometheus.NewGauge(prometheus.GaugeOpts{
				Namespace: "lmp",
				Subsystem: "masterserver",
				Name:      "servers_ipv6",
				Help:      "Number of IPv6-enabled servers",
			}),
			uniqueIPv4Addr: prometheus.NewGauge(prometheus.GaugeOpts{
				Namespace: "lmp",
				Subsystem: "masterserver",
				Name:      "unique_ipv4_addresses_count",
				Help:      "Number of unique IPv4 addresses",
			}),
			uniqueIPv6Addr: prometheus.NewGauge(prometheus.GaugeOpts{
				Namespace: "lmp",
				Subsystem: "masterserver",
				Name:      "unique_ipv6_addresses_count",
				Help:      "Number of unique IPv6 addresses",
			}),
			onlinePlayers: prometheus.NewGauge(prometheus.GaugeOpts{
				Namespace: "lmp",
				Subsystem: "masterserver",
				Name:      "players_online_total",
				Help:      "Total numbers of players online",
			}),
		},
	}
}

func (msc *Collector) Describe(c chan<- *prometheus.Desc) {
	msc.metrics.serversTotal.Describe(c)
	msc.metrics.serversWithIPv6.Describe(c)
	msc.metrics.uniqueIPv4Addr.Describe(c)
	msc.metrics.uniqueIPv6Addr.Describe(c)
	msc.metrics.onlinePlayers.Describe(c)
}

func (msc *Collector) Collect(c chan<- prometheus.Metric) {
	// TODO reset
	msc.metrics.onlinePlayers.Set(float64(0))

	err := msc.collectMetrics()
	if err != nil {
		err = fmt.Errorf("error during master server collect for %s: %w", msc.URL.String(), err)
		log.Println(err)
		return
	}
	msc.metrics.serversTotal.Collect(c)
	msc.metrics.serversWithIPv6.Collect(c)
	msc.metrics.uniqueIPv4Addr.Collect(c)
	msc.metrics.uniqueIPv6Addr.Collect(c)
	msc.metrics.onlinePlayers.Collect(c)
}

func (msc *Collector) collectMetrics() error {
	body, err := common.FetchJSON(msc.URL)
	if err != nil {
		return err
	}
	serverList := make(ServerList, 0, 128)
	err = json.NewDecoder(body).Decode(&serverList)
	if err != nil {
		return err
	}

	// Player count
	var playerCount int

	// Unique IP addresses
	var ip4s = make(map[string]struct{}, len(serverList))
	var ip6s = make(map[string]struct{}, len(serverList))

	// IPv6-enabled counter
	var ipv6Enabled int

	// Countries
	var countries = make(map[string]int, 32)

	for _, server := range serverList {
		host, _, err := net.SplitHostPort(server.ExternalEndpoint)
		if err == nil {
			_, exists := ip4s[host]
			if !exists {
				ip4s[host] = struct{}{}
			}
		}
		host6, _, err := net.SplitHostPort(server.InternalEndpoint6)
		if err != nil {
			// Bug in LMP, IPv6 addresses in JSON are not bracket-enclosed
			lIndex := strings.LastIndex(server.InternalEndpoint6, ":")
			if lIndex > 0 {
				host6 = server.InternalEndpoint6[:lIndex]
			}
		}
		if host6 != "" {
			_, exists := ip6s[host6]
			if !exists {
				ip6s[host6] = struct{}{}
			}
			// And if it's not ::1 it's IPv6-enabled!
			if host6 != "::1" {
				ipv6Enabled++
			}
		}

		country := server.Country
		if country == "" {
			country = "--"
		}
		countries[country]++
		playerCount += server.PlayerCount
	}
	msc.metrics.onlinePlayers.Set(float64(playerCount))
	msc.metrics.uniqueIPv4Addr.Set(float64(len(ip4s)))
	msc.metrics.uniqueIPv6Addr.Set(float64(len(ip6s)))
	msc.metrics.serversWithIPv6.Set(float64(ipv6Enabled))

	for country, count := range countries {
		msc.metrics.serversTotal.With(prometheus.Labels{"country": country}).Set(float64(count))
	}
	return nil
}
