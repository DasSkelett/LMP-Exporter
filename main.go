package main

import (
	"context"
	"log"
	"net/http"

	"gitlab.com/DasSkelett/LMP-Exporter/masterserver"
	"gitlab.com/DasSkelett/LMP-Exporter/server"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

/*
Example requests:
	http://localhost:9813/debug/probe?module=masterserver
	http://localhost:9813/probe?module=masterserver&target=http://servers.lunamultiplayer.com:8751/json
*/

type contextKey string

//goland:noinspection HttpUrlsUsage
const (
	debugServerURL      = "http://112.213.151.66:8900"
	debugMasterSeverURL = "http://servers.lunamultiplayer.com:8751/json"
)

func main() {
	probeHandler := getProbeHandler()
	debugHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		r = r.WithContext(context.WithValue(r.Context(), contextKey("debug"), true))
		probeHandler(w, r)
	})

	http.Handle("/metrics", promhttp.Handler())
	http.Handle("/probe", probeHandler)
	http.Handle("/debug/probe", debugHandler)

	log.Println("Starting HTTP server on :9813")
	log.Fatal(http.ListenAndServe(":9813", nil))
}

func getProbeHandler() http.HandlerFunc {
	masterServerProbeHandler := getMasterServerProbeHandler()
	serverProbeHandler := getServerProbeHandler()

	return func(w http.ResponseWriter, r *http.Request) {
		args := r.URL.Query()
		module := args.Get("module")

		if module == "masterserver" {
			masterServerProbeHandler.ServeHTTP(w, r)
		} else if module == "server" {
			serverProbeHandler.ServeHTTP(w, r)
		} else {
			http.Error(w, "module parameter is invalid", http.StatusBadRequest)
		}
	}
}

func getMasterServerProbeHandler() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		args := r.URL.Query()
		target := args.Get("target")
		debug, ok := r.Context().Value(contextKey("debug")).(bool)
		if !ok {
			debug = false
		}

		// The registry must be recreated for each run so old collectors don't accumulate
		registry := prometheus.NewRegistry()
		handler := promhttp.HandlerFor(registry, promhttp.HandlerOpts{Registry: registry})

		if target == "" {
			if debug {
				//goland:noinspection HttpUrlsUsage
				url := debugMasterSeverURL
				col := masterserver.NewMasterServerCollector(url)
				err := registry.Register(col)
				if err != nil {
					log.Println(err.Error())
					http.Error(w, "Could not register master server collector", http.StatusInternalServerError)
				}
			} else {
				http.Error(w, "Target parameter is missing", http.StatusBadRequest)
				return
			}
		} else {
			col := masterserver.NewMasterServerCollector(target)
			err := registry.Register(col)
			if err != nil {
				log.Println(err.Error())
				http.Error(w, "Could not register master server collector", http.StatusInternalServerError)
			}
		}
		handler.ServeHTTP(w, r)
	}
}

func getServerProbeHandler() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		args := r.URL.Query()
		target := args.Get("target")
		debug, ok := r.Context().Value("debug").(bool)
		if !ok {
			debug = false
		}

		// The registry must be recreated for each run so old collectors don't accumulate
		registry := prometheus.NewRegistry()
		handler := promhttp.HandlerFor(registry, promhttp.HandlerOpts{Registry: registry})

		if target == "" {
			if debug {
				url := debugServerURL
				col := server.NewServerCollector(url)
				if col == nil {
					log.Println("Invalid debug url " + url)
					http.Error(w, "Invalid debug url "+url, http.StatusInternalServerError)
				}
				err := registry.Register(col)
				if err != nil {
					log.Println(err.Error())
					http.Error(w, "Could not register server collector", http.StatusInternalServerError)
				}
			} else {
				http.Error(w, "Target parameter is missing", http.StatusBadRequest)
				return
			}
		} else {
			col := server.NewServerCollector(target)
			if col == nil {
				log.Println("Invalid target url " + target)
				http.Error(w, "Invalid target URL", http.StatusBadRequest)
			}
			err := registry.Register(col)
			if err != nil {
				log.Println(err.Error())
				http.Error(w, "Could not register server collector", http.StatusInternalServerError)
			}
		}
		handler.ServeHTTP(w, r)
	}
}
