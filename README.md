# LMP Exporter

[Prometheus](https://prometheus.io) exporter for [Luna Multiplayer](https://lunamultiplayer.com) master servers and servers.
Luna Multiplayer ([GitHub](https://github.com/LunaMultiplayer/LunaMultiplayer)) is a mod for [Kerbal Space Program](https://kerbalspaceprogram.com), adding multiplayer functionality to the game.

License: [MPL-2.0](https://www.mozilla.org/en-US/MPL/2.0/)

Example Prometheus `scrape_configs` entry:
```yaml
scrape_configs:
  - job_name: "lmp_masterserver"
    scrape_interval: 240s
    scrape_timeout: 8s

    metrics_path: /probe
    params:
      module: [masterserver]

    static_configs:
      - targets:
          - http://servers.lunamultiplayer.com:8701/json
          - http://servers.lunamultiplayer.com:8751/json

    relabel_configs:
      - source_labels: [__address__]
        target_label:  __param_target
      - source_labels: [__param_target]
        target_label:  instance
      - target_label:  __address__
        replacement:   localhost:2112
```
