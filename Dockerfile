FROM golang:1.22-alpine3.19 as build

EXPOSE 9813
WORKDIR /lmp-exporter
COPY go.mod .
COPY go.sum .
RUN go mod download
COPY . /lmp-exporter
RUN go test -vet=off ./...
RUN go build -o /go/bin/lmp-exporter
CMD [ "lmp-exporter" ]

FROM alpine:3.19
COPY --from=build /go/bin/lmp-exporter /usr/local/bin/
CMD [ "lmp-exporter" ]
