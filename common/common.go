package common

import (
	"io"
	"net/http"
	"net/url"
)

func FetchJSON(url url.URL) (io.Reader, error) {
	response, err := http.Get(url.String())
	if err != nil {
		return nil, err
	}
	return response.Body, nil
}
